package com.server;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.Dao.DaoTest;
import com.alibaba.fastjson.JSON;
import com.bean.DataBaseBean;
import com.bean.ReceivedBean;

public class DataTest {
	private int i;
	DaoTest daoTest;
	public DataTest(DaoTest daoTest){
		this.i=1;
		this.daoTest=daoTest;
	}
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	/**
	 * 服务层：处理Middle传入的
	 * @param jsoning
	 * @return 
	 */
	public String test(ReceivedBean received){
		try {
			if(i==1){
				List<DataBaseBean>listt=daoTest.query(received.getUserId());
				if(listt.size()>0){
					DataBaseBean bean=new DataBaseBean(received.getUserId(),received.getOtherId(),received.getLatitude(),
							received.getLongitude(),received.getRadius(),received.getAddr(),
							received.getLocationDescribe(),received.getSpeed(),received.getHeight(),
							date(new Date()));
					daoTest.update(bean,listt.get(0).getId());
					setI(0);
				}else{
					DataBaseBean bean=new DataBaseBean(received.getUserId(),received.getOtherId(),received.getLatitude(),
							received.getLongitude(),received.getRadius(),received.getAddr(),
							received.getLocationDescribe(),received.getSpeed(),received.getHeight(),
							date(new Date()));
					daoTest.add(bean);
					setI(0);
				}
			}else{
				DataBaseBean bean=new DataBaseBean(received.getUserId(),received.getOtherId(),received.getLatitude(),
						received.getLongitude(),received.getRadius(),received.getAddr(),
						received.getLocationDescribe(),received.getSpeed(),received.getHeight(),
						date(new Date()));
				daoTest.update(bean);
			}

				List<DataBaseBean>list= daoTest.query(received.getOtherId());
				if(list.size()>0){
					if(list.get(0).getDate().equals(date(new Date()))){
						ReceivedBean rece=new ReceivedBean(received.getUserId(),list.get(0).getUserId(),list.get(0).getLatitude(),
								list.get(0).getLongitude(),list.get(0).getRadius(), 
								list.get(0).getAddr(), list.get(0).getLocationDescribe(),
								list.get(0).getSpeed(), list.get(0).getHeight());
						return JSON.toJSONString(rece);
					}else{
						ReceivedBean rece=new ReceivedBean(received.getUserId(),list.get(0).getUserId(), list.get(0).getLatitude(),
								list.get(0).getLongitude(),list.get(0).getRadius(), 
								list.get(0).getDate()+list.get(0).getAddr(), list.get(0).getLocationDescribe(),
								list.get(0).getSpeed(), list.get(0).getHeight());
						return JSON.toJSONString(rece);
					}
				}else{
					String re="对方还没有使用过OnLine~";
					return re;
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 时间处理方法
	 * @param date
	 * @return
	 */
	private String date(Date date){
		   /** 输出格式: 2014-5-05 00:00:00 大写H为24小时制 */  
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm");  
        String s = sdf.format(date);  
		return s;
	}
	/**
	 * 主函数
	 * @param args
	 */
//	public static void main(String args[]){
//		DataTest dataTest=new DataTest();
//		DaoTest daoTest=new DaoTest();
//		if(daoTest.query("yueshutong").size()>0){
//			System.out.println("kkk"+daoTest.query("yueshutong").get(0).getAddr());
//		}
//		String s=dataTest.date(new Date());
//		DataBaseBean tuser=new DataBaseBean("yueshutong",9976,
//				9978,67,"buzhidao",
//				"ddsdsf","","",
//				dataTest.date(new Date()));
//		int i=daoTest.update(tuser, 1);
//		System.out.println(i);
//		daoTest.closeSession();
//	}
}
